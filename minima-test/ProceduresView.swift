//
//  ProceduresView.swift
//  minima-test
//
//  Created by Jordan Meek on 7/29/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import SwiftUI

struct ProceduresView: View {
    var airportChoice: String
    
    var body: some View {
        
        
        Group {
            if airportChoice == "KPWM" {
                let barTitle = "KPWM Approaches"
                List {
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "I11", title: "ILS RWY 11")) {
                        Text("ILS RWY 11")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "I29", title: "ILS RWY 29")) {
                        Text("ILS RWY 29")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "L11", title: "LOC RWY 11")) {
                        Text("LOC RWY 11")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "L29", title: "LOC RWY 29")) {
                        Text("LOC RWY 29")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "R11", title: "RNAV (GPS) RWY 11")) {
                        Text("RNAV (GPS) RWY 11")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "R18", title: "RNAV (GPS) RWY 18")) {
                        Text("RNAV (GPS) RWY 18")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "R29", title: "RNAV (GPS) RWY 29")) {
                        Text("RNAV (GPS) RWY 29")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    
                    NavigationLink(destination: TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "R36", title: "RNAV (GPS) RWY 36")) {
                        Text("RNAV (GPS) RWY 36")
                    }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                }
            } else {
                let barTitle = "EDDF Approaches"
                List {
                    Group {
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I07C", title: "ILS RWY 07C")) {
                            Text("ILS RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                        
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "L07C", title: "LOC RWY 07C")) {
                            Text("LOC RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I25C", title: "ILS RWY 25C")) {
                            Text("ILS RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                        
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "L25C", title: "LOC RWY 25C")) {
                            Text("LOC RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I25L", title: "ILS RWY 25L")) {
                            Text("ILS RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "L25L", title: "LOC RWY 25L")) {
                            Text("LOC RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I07RX", title: "ILS X RWY 07R")) {
                            Text("ILS X RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "L07RX", title: "LOC X RWY 07R")) {
                            Text("LOC X RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I07LZ", title: "ILS Z RWY 07L")) {
                            Text("ILS Z RWY 07L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I07RZ", title: "ILS Z RWY 07R")) {
                            Text("ILS Z RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    }
                    Group {
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "L07RZ", title: "LOC Z RWY 07R")) {
                            Text("LOC Z RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                        
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I25RZ", title: "ILS_Z_RWY_25R")) {
                            Text("ILS_Z_RWY_25R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J07CY", title: "GLS Y RWY 07C")) {
                            Text("GLS Y RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J07LY", title: "GLS Y RWY 07L")) {
                            Text("GLS Y RWY 07L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J07RY", title: "GLS Y RWY 07R")) {
                            Text("GLS Y RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J25CY", title: "GLS Y RWY 25C")) {
                            Text("GLS Y RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J25LY", title: "GLS Y RWY 25L")) {
                            Text("GLS Y RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J25RY", title: "GLS Y RWY 25R")) {
                            Text("GLS Y RWY 25R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J07CZ", title: "GLS Z RWY 07C")) {
                            Text("GLS Z RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J07LZ", title: "GLS Z RWY 07L")) {
                            Text("GLS Z RWY 07L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    }
                    Group {
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J07RZ", title: "GLS Z RWY 07R")) {
                            Text("GLS Z RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J25CZ", title: "GLS Z RWY 25C")) {
                            Text("GLS Z RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J25LZ", title: "GLS Z RWY 25L")) {
                            Text("GLS Z RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "J25RZ", title: "GLS Z RWY 25R")) {
                            Text("GLS Z RWY 25R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I07LY", title: "ILS Y RWY 07L")) {
                            Text("ILS Y RWY 07L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "I25RY", title: "ILS Y RWY 25R")) {
                            Text("ILS Y RWY 25R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R07CY", title: "RNAV GPS Y RWY 07C")) {
                            Text("RNAV GPS Y RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R07RY", title: "RNAV GPS Y RWY 07R")) {
                            Text("RNAV GPS Y RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R25CY", title: "RNAV GPS Y RWY 25C")) {
                            Text("RNAV GPS Y RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R25LY", title: "RNAV GPS Y RWY 25L")) {
                            Text("RNAV GPS Y RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    }
                    Group {
                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R07CZ", title: "RNAV GPS Z RWY 07C")) {
                            Text("RNAV GPS Z RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R07RZ", title: "RNAV GPS Z RWY 07R")) {
                            Text("RNAV GPS Z RWY 07R")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R25CZ", title: "RNAV GPS Z RWY 25C")) {
                            Text("RNAV GPS Z RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "R25LZ", title: "RNAV GPS Z RWY 25L")) {
                            Text("RNAV GPS Z RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "S07C", title: "VOR RWY 07C")) {
                            Text("VOR RWY 07C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "S25C", title: "VOR RWY 25C")) {
                            Text("VOR RWY 25C")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)

                        NavigationLink(destination: TransitionAndMinimaView(airportID: "EDDF", procedureChoice: "S25L", title: "VOR RWY 25L")) {
                            Text("VOR RWY 25L")
                        }.navigationBarTitle("\(barTitle)", displayMode: .inline)
                    }
                }
            }
        }
    }
}

struct ProceduresView_Previews: PreviewProvider {
    static var previews: some View {
        ProceduresView(airportChoice: "test")
    }
}
