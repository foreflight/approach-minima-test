//
//  AirportsListView.swift
//  minima-test
//
//  Created by Jordan Meek on 7/29/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import SwiftUI

struct AirportsListView: View {
    var body: some View {
        List {
//            NavigationLink(
//                destination: /*@START_MENU_TOKEN@*/Text("Destination")/*@END_MENU_TOKEN@*/,
//                label: {
//                    /*@START_MENU_TOKEN@*/Text("Navigate")/*@END_MENU_TOKEN@*/
//                })
            Text("KPWM Approaches")
            Text("EDDF Approaches")
        }
    }
}

struct AirportsListView_Previews: PreviewProvider {
    static var previews: some View {
        AirportsListView()
    }
}
