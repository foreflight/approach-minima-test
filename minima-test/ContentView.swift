//
//  ContentView.swift
//  minima-test
//
//  Created by Jordan Meek on 7/29/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView() {
            List {
                NavigationLink(destination: ProceduresView(airportChoice: "KPWM").navigationBarTitle("Airport Procedures", displayMode: .inline)) {
                    Text("KPWM")
                }.navigationBarTitle("Procedure Advisor", displayMode: .inline)
                
                NavigationLink(destination: ProceduresView(airportChoice: "EDDF").navigationBarTitle("Airport Procedures", displayMode: .inline)) {
                    Text("EDDF")
                }.navigationBarTitle("Procedure Advisor", displayMode: .inline)
            }
            
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
