//
//  SectionHeaderView.swift
//  minima-test
//
//  Created by Jordan Meek on 8/7/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import SwiftUI

struct SectionHeaderView: View {
    var title: String
    
    var body: some View {
        HStack {
            Text(title)
            Spacer()
        }
        
        .frame(height: 35)
        .background(Color.gray)
        
    }
}

struct SectionHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        SectionHeaderView(title: "Hello")
    }
}
