//
//  FFApproachDigitalProcedureMinimumFactory.swift
//  minima-test
//
//  Created by Jordan Meek on 7/23/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import Foundation

class FFApproachDigitalProcedureMinimumFactory{
    
    static func createMinima(forAirportID airport:String, andApproachID approach:String)->[FFApproachDigitalProcedureMinimum]?{
        if airport == "KPWM" && approach == "I11"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_ILS_RWY_11minima()
        } else if airport == "KPWM" && approach == "L11"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_LOC_RWY_11minima()
        } else if airport == "KPWM" && approach == "R11"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_RNAV_RWY_11minima()
        } else if airport == "KPWM" && approach == "I29"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_ILS_RWY_29minima()
        } else if airport == "KPWM" && approach == "L29"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_LOC_RWY_29minima()
        } else if airport == "KPWM" && approach == "Visual 29"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_HARBOR_VISUAL_RWY_29minima()
        } else if airport == "KPWM" && approach == "R29"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_RNAV_RWY_29minima()
        } else if airport == "KPWM" && approach == "R18"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_RNAV_RWY_18minima()
        } else if airport == "KPWM" && approach == "R36"{
            return FFApproachDigitalProcedureMinimumFactory.createKPWM_RNAV_RWY_36minima()
        } else if airport == "EDDF" && approach == "I07C"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "L07C"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_LOC_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "I25C"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "L25C"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_LOC_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "I25L"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_RWY_25Lminima()
        } else if airport == "EDDF" && approach == "L25L"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_LOC_RWY_25Lminima()
        } else if airport == "EDDF" && approach == "I07RX"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_X_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "L07RX"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_LOC_X_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "I07LZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_Z_RWY_07Lminima()
        } else if airport == "EDDF" && approach == "I07RZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_Z_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "L07RZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_LOC_Z_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "I25RZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_Z_RWY_25Rminima()
        } else if airport == "EDDF" && approach == "J07CY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Y_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "J07LY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Y_RWY_07Lminima()
        } else if airport == "EDDF" && approach == "J07RY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Y_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "J25CY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Y_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "J25LY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Y_RWY_25Lminima()
        } else if airport == "EDDF" && approach == "J25RY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Y_RWY_25Rminima()
        } else if airport == "EDDF" && approach == "J07CZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Z_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "J07LZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Z_RWY_07Lminima()
        } else if airport == "EDDF" && approach == "J07RZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Z_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "J25CZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Z_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "J25LZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Z_RWY_25Lminima()
        } else if airport == "EDDF" && approach == "J25RZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_GLS_Z_RWY_25Rminima()
        } else if airport == "EDDF" && approach == "I07LY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_Y_RWY_07Lminima()
        } else if airport == "EDDF" && approach == "I25RY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_ILS_Y_RWY_25Rminima()
        } else if airport == "EDDF" && approach == "R07CY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Y_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "R07RY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Y_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "R25CY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Y_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "R25LY"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Y_RWY_25Lminima()
        } else if airport == "EDDF" && approach == "R07CZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Z_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "R07RZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Z_RWY_07Rminima()
        } else if airport == "EDDF" && approach == "R25CZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Z_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "R25LZ"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_RNAV_GPS_Z_RWY_25Lminima()
        } else if airport == "EDDF" && approach == "S07C"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_VOR_RWY_07Cminima()
        } else if airport == "EDDF" && approach == "S25C"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_VOR_RWY_25Cminima()
        } else if airport == "EDDF" && approach == "S25L"{
            return FFApproachDigitalProcedureMinimumFactory.createEDDF_VOR_RWY_25Lminima()
        }
        return nil
    }
    
    fileprivate static func createProcedureMinimum(Category aircraftApproachCategory: FFApproachDigitalProcedureAircraftApproachCategory?,
                                                   Type approachType: FFApproachDigitalProcedureType?, StraightIn straightInRunway: String?,
                                                   RVR minimumRunwayVisualRangeValue: Int?,
                                                   _ minimumRunwayVisualRangeUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   Visibility minimumVisibilityValue: String?,
                                                   _ minimumVisibilityUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   CMV minimumConvertedMeteorologicalVisibilityValue: String?,
                                                   _ minimumConvertedMeteorologicalVisibilityUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   Altitude minimumAltitudeValue: Int?,
                                                   _ minimumAltitudeUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   Height minimumHeightValue: Int?,
                                                   _ minimumHeightUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   RA minimumRadioAltimeterValue: Int?,
                                                   _ minimumRadioAltimeterHeightUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   Lights approachRunwayLightingSystemStatus: [FFApproachDigitalProcedureRunwayLightingSystemStatus]?,
                                                   RequiredCeiling requiredCeilingValue: Int?,
                                                   _ requiredCeilingUnit: FFApproachDigitalProcedureMinimumUnit?,
                                                   OCHReferenceDatum obstacleClearanceHeightReferenceDatum: FFApproachDigitalProcedureObstacleClearanceHeightReferenceDatum?,
                                                   OCH obstacleClearanceHeight: Int?,
                                                   OCA obstacleClearanceAltitude: Int?,
                                                   Authorized isAuthorized: Bool, Customer customer: String?,
                                                   SideStep sideStepRunway: String?,
                                                   Criteria designCriteria: FFApproachDigitalProcedureDesignCriteria?,
                                                   Rules operatingRules: FFApproachDigitalProcedureOperatingRules?,
                                                   Circling isCircleToLand: Bool,
                                                   Restrictions restrictions: [FFApproachDigitalProcedureRestriction]?,
                                                   Precision isPrecision: Bool,
                                                   CDFA isCdfa: Bool,
                                                   RNP rnpValue: Double?,
                                                   RemoteAltimeter remoteAltimeterSource: String?,
                                                   WithoutFix withoutFix: String?,
                                                   TextRestrictions textRestrictions: [String]?,
                                                   CirclingRange circlingNotAuthorizedMagRadialClockwiseRange: [(Int, Int)]?) -> FFApproachDigitalProcedureMinimum {
        
        let minimum = FFApproachDigitalProcedureMinimum()
        
        minimum.isAuthorized = isAuthorized
        minimum.customer = customer
        minimum.approachType = approachType
        minimum.straightInRunway = straightInRunway
        minimum.sideStepRunway = sideStepRunway
        minimum.aircraftApproachCategory = aircraftApproachCategory
        minimum.designCriteria = designCriteria
        minimum.operatingRules = operatingRules
        minimum.isCircleToLand = isCircleToLand
        minimum.restrictions = restrictions
        minimum.isPrecision = isPrecision
        minimum.isCdfa = isCdfa
        minimum.rnpValue = rnpValue
        minimum.remoteAltimeterSource = remoteAltimeterSource
        minimum.approachRunwayLightingSystemStatus = approachRunwayLightingSystemStatus
        minimum.withoutFix = withoutFix
        minimum.textRestrictions = textRestrictions
        minimum.circlingNotAuthorizedMagRadialClockwiseRange = circlingNotAuthorizedMagRadialClockwiseRange
        
        minimum.minimumRunwayVisualRangeValue = minimumRunwayVisualRangeValue
        minimum.minimumRunwayVisualRangeUnit = minimumRunwayVisualRangeUnit
        
        minimum.minimumVisibilityValue = minimumVisibilityValue
        minimum.minimumVisibilityUnit = minimumVisibilityUnit
        
        minimum.minimumConvertedMeteorologicalVisibilityValue = minimumConvertedMeteorologicalVisibilityValue
        minimum.minimumConvertedMeteorologicalVisibilityUnit = minimumConvertedMeteorologicalVisibilityUnit
        
        minimum.minimumAltitudeValue = minimumAltitudeValue
        minimum.minimumAltitudeUnit = minimumAltitudeUnit
        
        minimum.minimumHeightValue = minimumHeightValue
        minimum.minimumHeightUnit = minimumHeightUnit
        
        minimum.minimumRadioAltimeterValue = minimumRadioAltimeterValue
        minimum.minimumRadioAltimeterHeightUnit = minimumRadioAltimeterHeightUnit
        
        minimum.requiredCeilingValue = requiredCeilingValue
        minimum.requiredCeilingUnit = requiredCeilingUnit
        
        minimum.obstacleClearanceHeightReferenceDatum = obstacleClearanceHeightReferenceDatum
        minimum.obstacleClearanceHeight = obstacleClearanceHeight
        minimum.obstacleClearanceAltitude = obstacleClearanceAltitude
        
        return minimum
    }
    
    fileprivate static func createKPWM_ILS_RWY_11minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatII,
                                   StraightIn: "11",
                                   RVR: 1200, .feet,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 176, .feet,
                                   Height: 100, .feet,
                                   RA: 108, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "11",
                                   RVR: 600, .feet,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatI,
                                   StraightIn: "11",
                                   RVR: 1400, .feet,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 226, .feet,
                                   Height: 150, .feet,
                                   RA: 166, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.specialAuthorizationRequired], Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Requires specific OPSPEC, MSPEC, or LOA approval and use of HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 644, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }
    
    fileprivate static func createKPWM_LOC_RWY_11minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 3500, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 3500, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 5500, .feet,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 5500, .feet,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 440, .feet,
                                   Height: 364, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )

        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 624, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 644, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: "FINUS", TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }

    fileprivate static func createKPWM_RNAV_RWY_11minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 1800, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut, .clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lpv,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 276, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 456, .feet,
                                   Height: 380, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: 4500, .feet,
                                   Visibility: "7/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: 4500, .feet,
                                   Visibility: "7/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: 5500, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: 5500, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "11",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 520, .feet,
                                   Height: 444, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: nil,
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createKPWM_ILS_RWY_29minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut, .railOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut, .railOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut, .railOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut, .railOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatII,
                                   StraightIn: "29",
                                   RVR: 1200, .feet,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 156, .feet,
                                   Height: 100, .feet,
                                   RA: 146, .feet,
                                   Lights: [.alsOut, .railOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.specialAuthorizationRequired], Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Reduced lighting requires specific OPSPEC, MSPEC, or LOA approval and use of autoland or HUD to touchdown."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatI,
                                   StraightIn: "29",
                                   RVR: 1400, .feet,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 206, .feet,
                                   Height: 150, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut, .railOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.specialAuthorizationRequired], Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createKPWM_LOC_RWY_29minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: 5500, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: 5500, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createKPWM_HARBOR_VISUAL_RWY_29minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .visual,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 784, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: 3000, .feet, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createKPWM_RNAV_RWY_29minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 18 with Flight Director or Autopilot or HUD to DA."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lpv,
                                   StraightIn: "29",
                                   RVR: 4000, .feet,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 256, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 567, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: 2400, .feet,
                                   Visibility: "1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: 5500, .feet,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: 5500, .feet,
                                   Visibility: "1 1/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: 5000, .feet,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 580, .feet,
                                   Height: 524, .feet,
                                   RA: nil, nil,
                                   Lights: [.railOut, .alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "29",
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createKPWM_RNAV_RWY_18minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lpv,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 300, .feet,
                                   Height: 250, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lpv,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 300, .feet,
                                   Height: 250, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lpv,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 300, .feet,
                                   Height: 250, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lpv,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 300, .feet,
                                   Height: 250, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 477, .feet,
                                   Height: 427, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 477, .feet,
                                   Height: 427, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 477, .feet,
                                   Height: 427, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 477, .feet,
                                   Height: 427, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 690, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 690, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 690, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 690, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "18",
                                   RVR: nil, nil,
                                   Visibility: "2 1/2", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createKPWM_RNAV_RWY_36minima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lp,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 460, .feet,
                                   Height: 411, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lp,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 460, .feet,
                                   Height: 411, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lp,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1 1/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 460, .feet,
                                   Height: 411, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lp,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1 1/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 460, .feet,
                                   Height: 411, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 560, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 560, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 560, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1 3/8", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 560, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: false, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: nil,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: nil,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 620, .feet,
                                   Height: 544, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: nil,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 740, .feet,
                                   Height: 664, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: nil,
                                   StraightIn: "36",
                                   RVR: nil, nil,
                                   Visibility: "1 3/4", .statuteMiles,
                                   CMV: nil, nil,
                                   Altitude: 860, .feet,
                                   Height: 784, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .terps21, Rules: .farPart91, Circling: true, Restrictions: nil, Precision: false, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_ILS_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "07C",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "07C",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "07C",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 429, .feet,
                                   Height: 100, .feet,
                                   RA: 102, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_LOC_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_ILS_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "25C",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "25C",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatII,
                                   StraightIn: "25C",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 464, nil,
                                   Height: 100, .feet,
                                   RA: 98, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_LOC_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 2100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "25C",
                                   RVR: 2100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 446, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_ILS_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "25L",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "25L",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatII,
                                   StraightIn: "25L",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 462, .feet,
                                   Height: 100, .feet,
                                   RA: 95, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227').","RVR 750m when a Flight Director or Autopilot or HUD to DA is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227').","RVR 750m when a Flight Director or Autopilot or HUD to DA is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227').","RVR 750m when a Flight Director or Autopilot or HUD to DA is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227').","RVR 750m when a Flight Director or Autopilot or HUD to DA is not used."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')."], CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_LOC_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_ILS_X_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "07R",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "07R",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatII,
                                   StraightIn: "07R",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 428, nil,
                                   Height: 100, .feet,
                                   RA: 101, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, nil,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_LOC_X_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, nil,
                                   Height: 482, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, nil,
                                   Height: 482, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, nil,
                                   Height: 482, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, nil,
                                   Height: 482, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, nil,
                                   Height: 482, nil,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, nil,
                                   Height: 482, nil,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_ILS_Z_RWY_07Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "07L",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "07L",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatII,
                                   StraightIn: "07L",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 405, .feet,
                                   Height: 100, .feet,
                                   RA: 99, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatII,
                                   StraightIn: "07L",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 411, .feet,
                                   Height: 106, .feet,
                                   RA: 105, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatII,
                                   StraightIn: "07L",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 424, .feet,
                                   Height: 119, .feet,
                                   RA: 118, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatII,
                                   StraightIn: "07L",
                                   RVR: 400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 437, .feet,
                                   Height: 132, .feet,
                                   RA: 131, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'","W/o HUD/AP/FD:RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'","W/o HUD/AP/FD:RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'","W/o HUD/AP/FD:RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'","W/o HUD/AP/FD:RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient min 4.6% up to 3500'"], CirclingRange: nil)
        )
        
        return minima
    }

    fileprivate static func createEDDF_ILS_Z_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "07R",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "07R",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatII,
                                   StraightIn: "07R",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 428, .feet,
                                   Height: 100, .feet,
                                   RA: 101, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_LOC_Z_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .loc,
                                   StraightIn: "07R",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: [.glideSlopeOut], Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )

        return minima
    }

    fileprivate static func createEDDF_ILS_Z_RWY_25Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIb,
                                   StraightIn: "25R",
                                   RVR: 75, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: nil, nil,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: nil,
                                   Type: .ilsCatIIIa,
                                   StraightIn: "25R",
                                   RVR: 200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: nil, nil,
                                   Height: 50, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatII,
                                   StraightIn: "25R",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 453, .feet,
                                   Height: 100, .feet,
                                   RA: 103, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatII,
                                   StraightIn: "25R",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 457, .feet,
                                   Height: 104, .feet,
                                   RA: 107, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatII,
                                   StraightIn: "25R",
                                   RVR: 300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 470, .feet,
                                   Height: 117, .feet,
                                   RA: 121, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatII,
                                   StraightIn: "25R",
                                   RVR: 400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 483, .feet,
                                   Height: 130, .feet,
                                   RA: 143, .feet,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'","RVR 750m when Flight Director or Autopilot or HUD to DA is not used"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'","RVR 750m when Flight Director or Autopilot or HUD to DA is not used"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'","RVR 750m when Flight Director or Autopilot or HUD to DA is not used"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'","RVR 750m when Flight Director or Autopilot or HUD to DA is not used"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% up to 2000'"], CirclingRange: nil)
        )

        return minima
    }
    
    fileprivate static func createEDDF_GLS_Y_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Y_RWY_07Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Miassed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Y_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Y_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Y_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Y_RWY_25Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Z_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 529, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Z_RWY_07Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 4.6% until passing 3500'"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Z_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "07R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 528, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Z_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25C",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 564, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Z_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')","W/o HUD/AP/FD: RVR 750m"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 562, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["LACFT: DA(H) 589' (227')"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_GLS_Z_RWY_25Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .gls,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_ILS_Y_RWY_07Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["W/o HUD/AP/FD: RVR 750m","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 505, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "07L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 512, .feet,
                                   Height: 207, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_ILS_Y_RWY_25Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD to DA is not used","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD to DA is not used","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD to DA is not used","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 550, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.tdzOut,.clOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["RVR 750m when a Flight Director or Autopilot or HUD to DA is not used","Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 553, .feet,
                                   Height: 200, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .ilsCatI,
                                   StraightIn: "25R",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 558, .feet,
                                   Height: 205, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["Missed apch climb gradient mim 5.0% until passing 2000'"], CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Y_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 790, .feet,
                                   Height: 461, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 790, .feet,
                                   Height: 461, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Y_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 502, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 512, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 502, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 512, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Y_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 150, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 2200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 2200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Y_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 770, .feet,
                                   Height: 408, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 770, .feet,
                                   Height: 408, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 2100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 2100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Z_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 800, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 654, .feet,
                                   Height: 325, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 800, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 664, .feet,
                                   Height: 335, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 1000, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 692, .feet,
                                   Height: 363, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 1000, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 701, .feet,
                                   Height: 372, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 654, .feet,
                                   Height: 325, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 664, .feet,
                                   Height: 335, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 1700, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 692, .feet,
                                   Height: 363, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "07C",
                                   RVR: 1700, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 701, .feet,
                                   Height: 372, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 790, .feet,
                                   Height: 461, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 790, .feet,
                                   Height: 461, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 491, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07C",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 511, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Z_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 750, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 624, .feet,
                                   Height: 296, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["With TDZ & CL & HUD: RVR 650m."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 750, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 634, .feet,
                                   Height: 306, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["With TDZ & CL & HUD: RVR 700m."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 750, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 644, .feet,
                                   Height: 316, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 1100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 716, .feet,
                                   Height: 338, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["With TDZ & CL & HUD: RVR 700m."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 624, .feet,
                                   Height: 296, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 634, .feet,
                                   Height: 306, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 644, .feet,
                                   Height: 316, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "07R",
                                   RVR: 1800, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 716, .feet,
                                   Height: 338, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 502, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 512, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 502, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "07R",
                                   RVR: 2400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 512, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Z_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 800, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 693, .feet,
                                   Height: 329, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 800, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 336, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 900, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 707, .feet,
                                   Height: 343, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 900, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 714, .feet,
                                   Height: 350, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 693, .feet,
                                   Height: 329, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 700, .feet,
                                   Height: 336, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 707, .feet,
                                   Height: 343, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "25C",
                                   RVR: 1600, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 714, .feet,
                                   Height: 350, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 436, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 2200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25C",
                                   RVR: 2200, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 840, .feet,
                                   Height: 476, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_RNAV_GPS_Z_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 750, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 654, .feet,
                                   Height: 292, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["With TDZ & CL & HUD: RVR 650m."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 750, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 663, .feet,
                                   Height: 301, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["With TDZ & CL & HUD: RVR 700m."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 750, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 673, .feet,
                                   Height: 311, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: ["With TDZ & CL & HUD: RVR 700m."], CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 800, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 683, .feet,
                                   Height: 321, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 654, .feet,
                                   Height: 292, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 663, .feet,
                                   Height: 301, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 673, .feet,
                                   Height: 311, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnavVnav,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 683, .feet,
                                   Height: 321, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: true, CDFA: false, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1400, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 2100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .lnav,
                                   StraightIn: "25L",
                                   RVR: 2100, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 820, .feet,
                                   Height: 458, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_VOR_RWY_07Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "07C",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 481, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_VOR_RWY_07Rminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 472, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 472, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 472, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 800, .feet,
                                   Height: 472, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "07R",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 810, .feet,
                                   Height: 482, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_VOR_RWY_25Cminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "25C",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 850, .feet,
                                   Height: 486, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
    
    fileprivate static func createEDDF_VOR_RWY_25Lminima() -> Array<FFApproachDigitalProcedureMinimum> {
        var minima: [FFApproachDigitalProcedureMinimum] = []
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.operatingFully],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .a,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .b,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 1500, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .c,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        minima.append(
            createProcedureMinimum(Category: .d,
                                   Type: .vor,
                                   StraightIn: "25L",
                                   RVR: 2300, .meters,
                                   Visibility: nil, nil,
                                   CMV: nil, nil,
                                   Altitude: 830, .feet,
                                   Height: 468, .feet,
                                   RA: nil, nil,
                                   Lights: [.alsOut],
                                   RequiredCeiling: nil, nil, OCHReferenceDatum: nil, OCH: nil, OCA: nil, Authorized: true, Customer: nil, SideStep: nil, Criteria: .pansOps, Rules: .easaOpsNco, Circling: false, Restrictions: nil, Precision: false, CDFA: true, RNP: nil, RemoteAltimeter: nil,  WithoutFix: nil, TextRestrictions: nil, CirclingRange: nil)
        )
        
        return minima
    }
}
