//
//  FFApproachDigitalProcedureMinimum.swift
//  minima-test
//
//  Created by Jordan Meek on 7/23/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import UIKit

enum FFApproachDigitalProcedureType: String {
    case unknown = "Unknown", ilsCatI = "ILS", ilsCatII = "Cat II ILS", ilsCatIII = "Cat III ILS", ilsCatIIIa = "Cat IIIa ILS", ilsCatIIIb = "Cat IIIb ILS", ilsCatIIIc = "Cat IIIc ILS", loc = "LOC", ndb = "NDB", ndbDme = "NDB DME", lpv = "LPV", lnavVnav = "VNAV", lnav = "LNAV", lp = "LP", lda = "LDA", ldaGs = "LDA GS", rnp = "RNP", gls = "GLS", vor = "VOR", vorDme = "VOR DME", visual = "Visual"
}

enum FFApproachDigitalProcedureAircraftApproachCategory: String {
    case a = "Cat A", b = "Cat B", c = "Cat C", d = "Cat D", dl = "Cat DL", e = "Cat E", h = "Helicopter"
    
    static func category(forInteger integer:Int)->FFApproachDigitalProcedureAircraftApproachCategory?{
        switch integer {
            case 1:
                return .a
            case 2:
                return .b
            case 3:
                return .c
            case 4:
                return .d
            default:
            return nil
        }
    }
}

enum FFApproachDigitalProcedureDesignCriteria: Int {
    case terps20, terps21, pansOps
    func stringValue() -> String? {
        switch self {
        case .terps20, .terps21:
            return "TERPS"
        case .pansOps:
            return "PANS-OPS"
        default:
            return nil
        }
    }
}

enum FFApproachDigitalProcedureOperatingRules: String {
    case farPart135 = "FAR Part 135", farPart91 = "FAR Part 91", farPart121 = "FAR Part 121", easaOpsCat = "EASA-OPS (CAT)", easaOpsNcc = "EASA-OPS (NCC)", easaOpsNco = "EASA-OPS (NCO)", easaOpsSpo = "EASA-OPS (SPO)", jarOps = "JAR-OPS", military = "Military", companyTailored = "Company Tailored"
}

enum FFApproachDigitalProcedureMinimumUnit: String {
    case meters = "m", feet = "ft", statuteMiles = "mi", nauticalMiles = "nm", kiloMeters = "km"
}

enum FFApproachDigitalProcedureRunwayLightingSystemStatus: String {
    case operatingFully = "Full", railOut = "RAIL Out", alsOut = "ALS Out", hirlOut = "HIRL Out", odalsOut = "ODALS Out", tdzOut = "TDZ Out", clOut = "CL Out", ldinOut = "LDIN Out"
    func simplifiedValue() -> String? {
        switch self {
        case .operatingFully:
            return "full"
        case .tdzOut, .clOut:
            return "someOut"
        case .alsOut:
            return "allOut"
        default:
            return nil
        }
    }
}

enum FFApproachDigitalProcedureInfrastructureStatus: String  {
    case operatingFully = "Full", lomOut = "LOM Out", gsOut = "GS Out", gpsOut = "GPS Out"
}

enum FFApproachDigitalProcedureRestriction: String {
    case dayOnly = "Day Only", nightOnly = "Night Only", dmeRequired = "DME Required", withoutDme = "Without DME", glideSlopeOut = "Glide Slope Out", specialAuthorizationRequired = "Special Authorization Required", rvRNa = "RVR Not Authorized"
}

enum FFApproachDigitalProcedureObstacleClearanceHeightReferenceDatum {
    case runwayThreshold, airportElevation
}

class FFApproachDigitalProcedureMinimum: NSObject {
    var isAuthorized: Bool = true
    var customer: String?
    var approachType: FFApproachDigitalProcedureType?
    var straightInRunway: String?
    var sideStepRunway: String?
    var aircraftApproachCategory: FFApproachDigitalProcedureAircraftApproachCategory?
    var designCriteria: FFApproachDigitalProcedureDesignCriteria?
    var operatingRules: FFApproachDigitalProcedureOperatingRules?
    var isCircleToLand: Bool = false
    var restrictions: [FFApproachDigitalProcedureRestriction]? = []
    var isPrecision: Bool = false
    var isCdfa: Bool = false
    var rnpValue: Double?
    var remoteAltimeterSource: String?
    var approachRunwayLightingSystemStatus: [FFApproachDigitalProcedureRunwayLightingSystemStatus]? = []
    var withoutFix: String?
    var textRestrictions: [String]?
    var circlingNotAuthorizedMagRadialClockwiseRange: [(Int, Int)]? = []
    
    var minimumRunwayVisualRangeValue: Int?
    var minimumRunwayVisualRangeUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var minimumVisibilityValue: String?
    var minimumVisibilityUnit: FFApproachDigitalProcedureMinimumUnit?
   
    var minimumConvertedMeteorologicalVisibilityValue: String?
    var minimumConvertedMeteorologicalVisibilityUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var minimumAltitudeValue: Int?
    var minimumAltitudeUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var minimumHeightValue: Int?
    var minimumHeightUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var minimumRadioAltimeterValue: Int?
    var minimumRadioAltimeterHeightUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var requiredCeilingValue: Int?
    var requiredCeilingUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var obstacleClearanceHeightReferenceDatum: FFApproachDigitalProcedureObstacleClearanceHeightReferenceDatum?
    var obstacleClearanceHeight: Int?
    var obstacleClearanceHeightUnit: FFApproachDigitalProcedureMinimumUnit?
    
    var obstacleClearanceAltitude: Int?
    var obstacleClearanceAltitudeUnit: FFApproachDigitalProcedureMinimumUnit?
    
    // Removed "fileprivate ( set )" from all variables above for initial development.
    
    var userDefinedMinimumVisibility: String?
    var userDefinedMinimumVisibilityUnit: FFApproachDigitalProcedureMinimumUnit?
    var userDefinedMinimumAltitude: Int?
    var userDefinedMinimumAltitudeUnit: FFApproachDigitalProcedureMinimumUnit?
    
}
