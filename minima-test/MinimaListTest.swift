//
//  MinimaListTest.swift
//  minima-test
//
//  Created by Jordan Meek on 7/23/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import SwiftUI

let minima = FFApproachDigitalProcedureMinimumFactory.createMinima(forAirportID: "KPWM", andApproachID: "R11")

struct MinimaListTest: View {
    var body: some View {
        Form {
            VStack {
                ForEach(minima!, id: \.self) { minimum in
                    SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "no type", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "no cat", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? "no vis", minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
                }
            }
        }
    }
}

struct SelectedApproachMinimum: View {
    var designCriteria: String = "no criteria"
    var approachType: String
    var aircraftApproachCategory: String
    var textRestrictions: [String]
    var minimumRunwayVisualRangeValue: Int
    var minimumVisibilityValue: String?
    var minimumAltitudeValue: Int
    var minimumHeightValue: Int
    var isPrecision: Bool
    
    var body: some View {
        Group() {
//            HStack() {
//                VStack(alignment: .leading) {
//                    Text("MINIMUM (\(designCriteria))")
//                        .foregroundColor(Color.gray)
//                        .font(.subheadline)
//                }
//                Spacer()
//                VStack() {
//                    Text("")
//                }
//            }
            HStack(alignment: .top) {
                VStack(alignment: .leading, spacing:0) {
                    
                    HStack {
                        Text(approachType)
                        //Text(aircraftApproachCategory).font(.subheadline)
                    }
                    ForEach(textRestrictions, id: \.self) { textRestriction in
                        Text(textRestriction)
                            .foregroundColor(Color.red)
                            .font(.footnote)
                    }
                }
                Spacer()
                VStack(alignment: .trailing, spacing:0) {
                    if minimumRunwayVisualRangeValue == 0 && minimumVisibilityValue != nil {
                        Text("\(minimumVisibilityValue ?? "0")")
                        Text("mi")
                    } else if minimumRunwayVisualRangeValue != 0 && minimumVisibilityValue == nil {
                        Text("\(minimumRunwayVisualRangeValue)m")
                        Text("")
                            .font(.footnote)
                            .foregroundColor(Color.gray)
                    } else {
                        Text("\(minimumRunwayVisualRangeValue)'")
                        Text("\(minimumVisibilityValue ?? "VIS") mi")
                    }
                    
                }
                Spacer()
                    .frame(width: 10.0)
                VStack(spacing:0) {
                    HStack(alignment: .bottom, spacing:0) {
                        Text("\(minimumAltitudeValue)'")
                            .foregroundColor(Color.blue)
                            .fontWeight(.bold)
                        Group {
                            if isPrecision {
                                Text("DA    ")
                                    .font(.footnote)
                                    .foregroundColor(Color.gray)
                            } else {
                                Text("MDA ")
                                    .font(.footnote)
                                    .foregroundColor(Color.gray)
                            }
                        }.frame(width: 35)
                    }
                    HStack(alignment: .bottom, spacing:0) {
                        Text("\(minimumHeightValue)'")
                        Group {
                            if isPrecision {
                                Text("DH   ")
                                    .font(.footnote)
                                    .foregroundColor(Color.gray)
                            } else {
                                Text("MDH ")
                                    .font(.footnote)
                                    .foregroundColor(Color.gray)
                            }
                        }.frame(width: 35)
                    }
                }
//                HStack(alignment: .bottom) {
//                    VStack(alignment: .trailing, spacing:0) {
//                        if isPrecision {
//                            Text("DA")
//                                .font(.footnote)
//                            Text("DH")
//                                .font(.footnote)
//                        } else {
//                            Text("MDA")
//                                .font(.footnote)
//                            Text("MDH")
//                                .font(.footnote)
//                        }
//                    }
//                }
                
            }
            //.padding(6)
            //.foregroundColor(Color.gray)
            Divider()
        }
    }
}

struct MinimaListTest_Previews: PreviewProvider {
    static var previews: some View {
        MinimaListTest()
            .environment(\.colorScheme, .light)
    }
}
