//
//  TransitionAndMinimaView.swift
//  minima-test
//
//  Created by Jordan Meek on 7/29/20.
//  Copyright © 2020 Jordan Meek. All rights reserved.
//

import SwiftUI

func createFilteredMinima(minima: [FFApproachDigitalProcedureMinimum], aircraftCategory: String, runwayLightingStatus: String) -> Array<FFApproachDigitalProcedureMinimum> {
    var filteredMinima: [FFApproachDigitalProcedureMinimum] = []
    
    for minimum in minima {
        if minimum.aircraftApproachCategory?.rawValue == aircraftCategory {
            if let lightStatusArray = minimum.approachRunwayLightingSystemStatus {
                for runwayLightStatus in lightStatusArray {
                    if runwayLightStatus.simplifiedValue() == runwayLightingStatus {
                        filteredMinima.append(minimum)
                    }
                }
            }
        }
    }
    return filteredMinima
}

func greaterThanTwoLightsOutTypes(minima: [FFApproachDigitalProcedureMinimum]) -> Bool {
    var someOutIsSeen = false
    
    for minimum in minima {
        if let lightStatusArray = minimum.approachRunwayLightingSystemStatus {
            for lightStatus in lightStatusArray {
                if let simplifiedStatus = lightStatus.simplifiedValue() {
                    if simplifiedStatus == "someOut" {
                        someOutIsSeen = true
                    }
                }
            }
        }
    }
    
    return someOutIsSeen
}

struct TransitionAndMinimaView: View {
    @State private var aircraftCategory = "Cat B"
    @State private var runwayLightingSystemStatus = "full"
    @State private var userMinimumVisibility = ""
    @State private var userMinimumAltitude = ""
    var airportID: String
    var procedureChoice: String
    var title: String
    
    var body: some View {
        let minima = FFApproachDigitalProcedureMinimumFactory.createMinima(forAirportID: airportID, andApproachID: procedureChoice)
        
        // MARK: - User Override Input
        HStack {
            VStack(alignment: .center) {
                TextField("Vis", text: $userMinimumVisibility)
            }
            VStack(alignment: .center) {
                TextField("Alt", text: $userMinimumAltitude)
            }
        }
        
        // MARK: - Segmented control for aircraft catigory
        
        Picker(selection: $aircraftCategory, label: Text("Aircraft Category")) {
            Text("Cat A").tag("Cat A")
            Text("Cat B").tag("Cat B")
            Text("Cat C").tag("Cat C")
            Text("Cat D").tag("Cat D")
        }.pickerStyle(SegmentedPickerStyle())
        
        
        
        if aircraftCategory == "Cat A" {
            Text("Approach Speed <91kts")
                .font(.footnote)
                .foregroundColor(Color.gray)
        } else if aircraftCategory == "Cat B" {
            Text("Approach Speed 91-120kts")
                .font(.footnote)
                .foregroundColor(Color.gray)
        } else if aircraftCategory == "Cat C"{
            Text("Approach Speed 121-140kts")
                .font(.footnote)
                .foregroundColor(Color.gray)
        } else if aircraftCategory == "Cat D" {
            Text("Approach Speed 141-165kts")
                .font(.footnote)
                .foregroundColor(Color.gray)
        } else {
            Text("Error")
        }
        
        Picker(selection: $runwayLightingSystemStatus, label: Text("Runway Lighting System Status")) {
            if greaterThanTwoLightsOutTypes(minima: minima ?? []) {
                Text("Full Lighting").tag("full")
                Text("TDZ/CL Out").tag("someOut")
                Text("ALS Out").tag("allOut")
            } else {
                Text("Full Lighting").tag("full")
                Text("Approach Lights Out").tag("allOut")
            }
            
            
        }.pickerStyle(SegmentedPickerStyle())
        
        Divider()
        
        List {
            VStack {
                ForEach(createFilteredMinima(minima: minima ?? [], aircraftCategory: aircraftCategory, runwayLightingStatus: runwayLightingSystemStatus), id: \.self) { minimum in
                    SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "Circling", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? nil, minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
                    
                }
            }.navigationBarTitle("\(title) Minima", displayMode: .inline)
        }
        
        // MARK: - Section Header for each aircraft catigory
        
//        VStack {
//            ScrollView(.vertical) {
//                LazyVStack(alignment: .leading, spacing: nil, pinnedViews: [.sectionHeaders], content: {
//                    Section(header: SectionHeaderView(title: "Cat A")) {
//                        ForEach(minimaCatA, id: \.self) { minimum in
//                            SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "Circling", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? nil, minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
//
//                        }
//                    }
//                    Section(header: SectionHeaderView(title: "Cat B")) {
//                        ForEach(minimaCatB, id: \.self) { minimum in
//                            SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "Circling", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? nil, minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
//
//                        }
//                    }
//                    Section(header: SectionHeaderView(title: "Cat C")) {
//                        ForEach(minimaCatC, id: \.self) { minimum in
//                            SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "Circling", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? nil, minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
//
//                        }
//                    }
//                    Section(header: SectionHeaderView(title: "Cat D")) {
//                        ForEach(minimaCatD, id: \.self) { minimum in
//                            SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "Circling", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? nil, minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
//
//                        }
//                    }
//                })
//            }.navigationBarTitle("\(title) Minima", displayMode: .inline)
//        }
        
        
// MARK: - Single list of all minima
//        return List {
//            VStack {
//                ForEach(minima!, id: \.self) { minimum in
//                    SelectedApproachMinimum(designCriteria: minimum.designCriteria?.stringValue() ?? "no criteria", approachType: minimum.approachType?.rawValue ?? "Circling", aircraftApproachCategory: minimum.aircraftApproachCategory?.rawValue ?? "", textRestrictions: minimum.textRestrictions ?? [], minimumRunwayVisualRangeValue: minimum.minimumRunwayVisualRangeValue ?? 0, minimumVisibilityValue: minimum.minimumVisibilityValue ?? nil, minimumAltitudeValue: minimum.minimumAltitudeValue ?? 0, minimumHeightValue: minimum.minimumHeightValue ?? 0, isPrecision: minimum.isPrecision)
//                }
//            }.navigationBarTitle("\(title) Minima", displayMode: .inline)
//        }
    }
}

struct TransitionAndMinimaView_Previews: PreviewProvider {
    static var previews: some View {
        TransitionAndMinimaView(airportID: "KPWM", procedureChoice: "I11", title: "title")
    }
}
